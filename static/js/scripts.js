var client = new WebSocket('ws://' + window.location.host, 'echo-protocol');
 
client.onerror = function() {
    console.log('Connection Error')
};
 
client.onopen = function() {
    console.log('WebSocket Client Connected')

    client.send("were open")

    $(document).keypress(function(event){
        client.send(event.charCode)
    })
}
 
client.onclose = function() {
    console.log('echo-protocol Client Closed');
}
 
client.onmessage = function(e) {
    if (typeof e.data === 'string') {
        console.log("Received: '" + e.data + "'")
    }
}